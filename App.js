/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React from 'react';
import Root from './src/Container/Navigation/router';


function App() {
  return (
    <Root />
  );
}

export default App;
