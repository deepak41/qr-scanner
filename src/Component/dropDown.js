import React from 'react';

import {View, StyleSheet,Text, Image} from 'react-native';

import {Dropdown} from 'react-native-material-dropdown-v2';
import Color from "../common/Color";

const rightArrow = require("../common/assets/downArrow.png")

export default class TFTDropDown extends React.Component {
  render() {
    let {
      container = false,
      title,
      data = [],
      containerStyle = {},
      value,
      onChange,
      selectedItemColor = Color.textPrimary,
      disabled,
      baseColor = 'grey',
      onPress,
      errorShow,
      errorMessage,
      isMandatory = false,
    } = this.props;
    return (
      <View style={container ? {flexDirection:'row'} : {flex: 1}}>
        <Dropdown
          disabled={disabled}
          label={title}
          data={data || []}
          value={value}
          onPress={onPress}
          containerStyle={containerStyle}
          onChangeText={(value, index) => onChange(value, data[index])}
          textColor={ errorShow && errorMessage && errorMessage != "" ? 'red': selectedItemColor}
          isMandatory={isMandatory}
          // baseColor={baseColor}
          baseColor={errorShow && errorMessage && errorMessage != "" ? 'red':'#ADADAD'}
          selectedItemColor={Color.textPrimary}
          // error={errorShow ? errorMessage ? errorMessage :'' : ''}
          // renderRightAccessory={() => <Image  source={rightArrow}  style={styles.arrowstyle} />}
        />
        <Image  source={rightArrow}  style={styles.arrowstyle} />
                          {errorShow && errorMessage && errorMessage != "" ?<Text style={styles.errorMessage}>{errorMessage}</Text>:null} 
        
      </View>
    );
  }
}

const styles = StyleSheet.create({
  arrowstyle: {
    marginTop: 8,
    padding:5,
    height:26,
    width:26,
    marginLeft:-20,
    marginTop:30
  },
  errorMessage: {
    color: 'red',
     marginTop: -5,
    fontSize:12
  }
});
