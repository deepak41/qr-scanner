import React, {Fragment} from 'react';

import {View, Text, TouchableOpacity, StyleSheet, StatusBar, Platform, Image} from 'react-native';
import Color from '../common/Color';
import Constant from '../common/Constant';
const back = require("../common/assets/back.png")
// import Images from '@common/Images';
// import Fonts from '@common/fonts';
// import {Colors} from 'react-native/Libraries/NewAppScreen';
// const {Montserratsemibold} = Fonts.fonts;

export default class NavBar extends React.Component {
  componentDidMount() {
    if (Platform.OS === 'android') StatusBar.setBackgroundColor(Color.primary);
  }

  render() {
    let {extra, title, goBack, hasBack, rightItem, subtitle, headerStyle, username, titleItem} = this.props;
    // const {Back, TraformLogo} = Images.icons;
    return (
      <View style={[styles.container, headerStyle != null ? headerStyle : {}]}>
        <StatusBar backgroundColor={Color.primary} />
        <Fragment>
          <View style={styles.navContainer}>
            {hasBack ? (
              <TouchableOpacity onPress={() => goBack && goBack()} style={styles.back}>
                {/* <Back width={20} height={20} color="white" /> */}
                <Image source={back} style={[{tintColor:"#FFF",height:32,width:32}]} />
              </TouchableOpacity>
            ) : (
              <View style={styles.image} />
                // <Image source={back} style={[{tintColor:"#FFF",height:32,width:32},styles.image]} />
            //   <TraformLogo width={32} height={32} style={styles.image} />
            )}
            <View style={{flex: 1, justifyContent: 'center', width: '100%'}}>
              <View style={{flexDirection: 'row'}}>
                <Text style={styles.title} numberOfLines={1}>
                  {title}
                </Text>
                {titleItem && <>{titleItem}</>}
              </View>
              {subtitle != null && subtitle != '' && <Text style={[styles.title, {color: 'white', fontSize: 14, marginBottom: 0}]}>{subtitle}</Text>}
            </View>
            {/* {rightItem} */}
          </View>
          <View style={Platform.OS === 'android' ? styles.rightDroid : styles.rightIos}>{rightItem}</View>
        </Fragment>
        {extra}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: Color.primary,
    paddingTop: Constant.ToolbarHeight,
    zIndex: 1000,
  },
  navContainer: {
    height: Constant.navBarHeight,
    width: '100%',
    alignItems: 'center',
    paddingRight: 10,
    flexDirection: 'row',
    justifyContent: 'center',
  },
  title: {
    fontSize: 18,
    color: 'white',
    marginLeft: 0,
  },
  back: {
    padding: 15,
    justifyContent: 'center',
  },
  image: {
    resizeMode: 'contain',
    marginLeft: 20,
    marginRight: 10,
  },
  rightIos: {
    height: '100%',
    position: 'absolute',
    flexDirection: 'row',
    alignSelf: 'flex-end',
    zIndex: 1,
    marginTop: 40,
    right: 8
  },
  rightDroid: {
    height: '100%',
    position: 'absolute',
    flexDirection: 'row',
    alignSelf: 'flex-end',
    right: 8
  },
});
