import React from 'react';
import {View, Text, TouchableOpacity, SectionList, ScrollView,StyleSheet,Linking, Dimensions} from 'react-native';
import QRCodeScanner from 'react-native-qrcode-scanner';
import { RNCamera } from 'react-native-camera';
import Navbar from '../../Component/navbar';
import AsyncStorage from '@react-native-community/async-storage';


class Home extends React.Component{

  state = {
    count:null
  }

  onSuccess = e => {
    const {navigation} = this.props
    console.log("data",e)
    navigation.push("Next",{qrCodeData:e})
  };
  renderListView = () =>{
    const {count} = this.state
    AsyncStorage.getItem("BOARDED").then(item=>{
      this.setState({count:item})
  })
    return(      
        // <ScrollView horizontal={true} style={styles.rowlistview}>  
        //       {this.list()}
        // </ScrollView>
        <View style={{flexDirection:'row',justifyContent:"space-between"}}>
            <View style={styles.cardMainView}>
             <Text>{"Total Seats"}</Text>
             <Text style={styles.txtStyle}>{"40"}</Text>
         </View>
         <View style={styles.cardMainView}>
             <Text>{"Booked"}</Text>
             <Text style={styles.txtStyle}>{"40"}</Text>
         </View>
         <View style={styles.cardMainView}>
             <Text>{"Boarded"}</Text>
             <Text style={styles.txtStyle}>{count ? (eval(count)+10).toString() : "10"}</Text>
             {/* <Text>{"10"}</Text> */}
         </View>
        </View>
    )
}

  
     render(){
       const {navigation} = this.props;
         return(
           <View style={styles.container}>
             <Navbar title={"Home"} hasBack={true} goBack={()=>navigation.replace("Launch")} />
                 <View style={styles.content}><QRCodeScanner
        onRead={this.onSuccess}
         cameraStyle={{height:"100%"}}
        // containerStyle={{height:Dimensions.get('screen').height}}
        fadeIn={true}
        showMarker={true}
        markerStyle={{borderRadius:22,
        borderWidth:4,        
        overflow:'scroll',
        borderColor:"#FFF"
      }}
        // ref={(node) => { this.scanner = node }}
        // flashMode={RNCamera.Constants.FlashMode.torch}
        //  reactivate={true},

        topContent={
          <Text style={styles.centerText}>
            Please Scan the QR code.
          </Text>
        }
        bottomContent={
         this.renderListView()
        }
      /></View>
           </View>
         )
     }
}

const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: "#FFF",
      borderTopLeftRadius: 20,
      borderTopRightRadius: 20,
    },
    centerText: {
      //  flex: 1,
      fontSize: 18,
      padding: 32,
      color: '#777',
      marginBottom:100
    },
    textBold: {
      fontWeight: '500',
      color: '#000'
    },
    buttonText: {
      fontSize: 21,
      color: 'rgb(0,122,255)'
    },
    buttonTouchable: {
      padding: 16
    },
    content: {
      
     // backgroundColor: 'white',
       //flex: 1,
       height:"90%"
    },
    cardMainView:{
      marginHorizontal:8,
      // marginVertical:16,
      shadowColor: 'black',
      shadowOffset: { width: 1, height: 1 },
      shadowOpacity: 0.4,
      shadowRadius: 3,
      elevation: 7,
      backgroundColor: 'white',
      borderRadius: 10,
      marginVertical: 5,
      padding:16,
      marginTop:78,
      height:80       
    },
    txtStyle:{
      textAlign:"center"
  }
  });

export default Home;