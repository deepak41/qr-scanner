import React from 'react';
import {View, Text, TouchableOpacity, SectionList, ScrollView,StyleSheet,Linking, Image, Animated, Dimensions} from 'react-native';
import QRCodeScanner from 'react-native-qrcode-scanner';
import { RNCamera } from 'react-native-camera';
import Navbar from '../../Component/navbar';
import Color from '../../common/Color';
import SwipeButton from 'rn-swipe-button';
import moment from 'moment';
import DropDown from "../../Component/dropDown";
import AsyncStorage from '@react-native-community/async-storage';


class Launch extends React.Component{

    state={
        listArray:[
            {
                id:1,
                title:"Total Seats",
                no:"40"
            },
            {
                id:2,
                title:"Booked",
                no:"40"
            },
            {
                id:3,
                title:"Boarded",
                no:""
            },
        ],
        flightValues:"",
        isFlightVisible:false,
        count:null
    }

    list = () =>{
        const {listArray} = this.state;
       return listArray.map(item =>{
           return(
             <View style={styles.cardMainView}>
                 <Text>{item.title}</Text>
                 <Text>{item.no}</Text>
             </View>
           )
       })
    }

    renderListView = () =>{
        const {count} = this.state
        let getBoarded = null
           AsyncStorage.getItem("BOARDED").then(item=>{
               this.setState({count:item})
           })
        return(      
            <View style={{flexDirection:'row',justifyContent:"space-between"}}>
                <View style={styles.cardMainView}>
                 <Text>{"Total Seats"}</Text>
                 <Text style={styles.txtStyle}>{"40"}</Text>
             </View>
             <View style={styles.cardMainView}>
                 <Text>{"Booked"}</Text>
                 <Text style={styles.txtStyle}>{"40"}</Text>
             </View>
             <View style={styles.cardMainView}>
                 <Text>{"Boarded"}</Text>
                 <Text style={styles.txtStyle}>{count ? (eval(count)+10).toString() : "10"}</Text>
                 {/* <Text>{"10"}</Text> */}
             </View>
            </View>
        )
    }

    onValueChange = (value) =>{
         this.setState({flightValues:value})
    }

    renderMainView = () =>{
        const {flightValues,isFlightVisible} = this.state;
        const {navigation} = this.props; 
        return(
            <View style={styles.mainView}>
                  {flightValues == "" && <TouchableOpacity style={styles.btnView} onPress={()=>this.setState({isFlightVisible:true})}>
                          <Text style={styles.btnTxt}>{`START Boarding`}</Text>
                   </TouchableOpacity>}
                   
                   {isFlightVisible ? <DropDown title={"Flight Time"} data={[{label:"4:00 QQ345", value:1},{label:"8:00 QQ6475", value:2}]} containerStyle={{width:Dimensions.get("screen").width-25}} container={true}
                   onChange={(value,index)=>this.onValueChange(value)}
                   />:null} 
                   {flightValues != "" && this.renderListView()}

                  {flightValues != "" && <TouchableOpacity style={[styles.btnView, styles.bottomButton]} onPress={()=>navigation.replace("Home")}>
                          <Text style={styles.btnTxt}>{`START Boarding`}</Text>
                   </TouchableOpacity>} 
             </View>
        )
    }
    render(){
        return(
        <View style={styles.container}>
                         <Navbar title={"Launch Boarding"} hasBack={false} />
             {this.renderMainView()}
        </View>
        )
    }
}

const styles = StyleSheet.create({
     container:{
         flex:1
     },
     mainView:{
         flex:1,
         justifyContent:"center",
         alignItems:'center',
         paddingHorizontal:16
     },
     btnView:{
       backgroundColor:Color.primary,
       borderRadius:8,
       alignItems:"center",
       justifyContent:"center",
       paddingHorizontal:30,
       paddingVertical:20,
       shadowColor: 'black',
      shadowOffset: {width: 1, height: 1},
      shadowOpacity: 0.4,
      shadowRadius: 3,
      elevation: 3,
      height:50,
      marginBottom:30
     },
     btnTxt:{
         color:"#FFF"
     },
     cardMainView:{
        marginHorizontal:8,
        // marginVertical:16,
        shadowColor: 'black',
        shadowOffset: { width: 1, height: 1 },
        shadowOpacity: 0.4,
        shadowRadius: 3,
        elevation: 7,
        backgroundColor: 'white',
        borderRadius: 10,
        marginVertical: 5,
        padding:16,
        marginTop:78,
        height:80       
      },
      rowlistview:{
        flexDirection:'row',
        marginTop:15,
        alignSelf:'center',
    },
    bottomButton:{
        marginTop:30
    },
    txtStyle:{
        textAlign:"center"
    }
})

export default Launch;