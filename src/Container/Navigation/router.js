import { createAppContainer, createSwitchNavigator } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';

 import LaunchScreen from '../Launch';
import HomeScreen from '../Home';
import NextScreen from '../Next';

const AuthStack = createStackNavigator(
    {
         Home:HomeScreen,
         Next:NextScreen,
          Launch:LaunchScreen
    },
    {
        initialRouteName: 'Launch',
    headerMode: 'none',
    mode: 'modal',
    defaultNavigationOptions: {
      gesturesEnabled: false,
    },

    }
)

export default createAppContainer(
    createSwitchNavigator(
      {
        Auth: AuthStack,
      },
      {
        initialRouteName: 'Auth',
        headerMode: 'none',
        mode: 'modal',
        defaultNavigationOptions: {
          gesturesEnabled: false,
        },
      },
    ),
  );