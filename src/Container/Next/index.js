import React from 'react';
import {View, Text, TouchableOpacity, SectionList, ScrollView,StyleSheet,Linking, Image, Animated} from 'react-native';
import QRCodeScanner from 'react-native-qrcode-scanner';
import { RNCamera } from 'react-native-camera';
import Navbar from '../../Component/navbar';
import Color from '../../common/Color';
import SwipeButton from 'rn-swipe-button';
import moment from 'moment';
import AsyncStorage from '@react-native-community/async-storage';

const completed = require('../../common/assets/Completed.png')

class Next extends React.Component{

  constructor(props){
    super(props)
    this.animatedValue = new Animated.Value(0)
    this.state = {
      isCompleted:false
    }
  }

   isJsonString =(str)=> {
    try {
        JSON.parse(str);
    } catch (e) {
        return false;
    }
    return true;
}

  renderTripInfo = () =>{
    const {navigation} = this.props;
    const qrCodeData = navigation.getParam('qrCodeData')
    const tripDetail = qrCodeData &&   qrCodeData.data && this.isJsonString(qrCodeData.data) ? JSON.parse(qrCodeData.data) : null
    const flightInfo= tripDetail && tripDetail["0"] ? tripDetail["0"] : null
    if (tripDetail) {
      return(
        <View style={styles.tripInfoMainView}>
            <View style={{alignItems:"center"}}>
            <Text style={styles.titleStyle}>{"Traveller Name"}</Text>         
            <Text style={styles.subTitleStyles}>{`${tripDetail &&  tripDetail.passengers && tripDetail.passengers.length > 0 && tripDetail.passengers[0] && tripDetail.passengers[0].title ? tripDetail.passengers[0].title:""} ${tripDetail && tripDetail.passengers && tripDetail.passengers.length > 0 && tripDetail.passengers[0] && tripDetail.passengers[0].first_name ?tripDetail.passengers[0].first_name :""} ${tripDetail && tripDetail.passengers && tripDetail.passengers.length > 0 && tripDetail.passengers[0] && tripDetail.passengers[0].last_name ?tripDetail.passengers[0].last_name : ""}`}</Text>
            </View>
            <View style={styles.horizontalLine} />
            <View style={styles.innerView}>
              <View>
            <Text style={styles.titleStyle}>{"Arrival City"}</Text>
            <Text style={styles.subTitleStyles}>{flightInfo && flightInfo.arrCity ?flightInfo.arrCity : ""}</Text>
            </View>
            <View style={styles.departureStyle}>
            <Text style={styles.titleStyle}>{"Departure City"}</Text>
            <Text style={styles.subTitleStyles}>{flightInfo && flightInfo.depCity ? flightInfo.depCity : ""}</Text>
            </View>
            </View>
            <View style={styles.horizontalLine} />
            <View style={styles.innerView}>
              <View>
            <Text style={styles.titleStyle}>{"Arrival Date"}</Text>
            <Text style={styles.subTitleStyles}>{flightInfo && flightInfo.arrivalDate && flightInfo.arrivalDate != "" ?  moment(new Date(flightInfo.arrivalDate)).format('DD MMM, YYYY'):""}</Text>
            </View>
            <View>
            <Text style={styles.titleStyle}>{"Departure Date"}</Text>
            <Text style={styles.subTitleStyles}>{flightInfo && flightInfo.departureDate && flightInfo.departureDate != "" ?  moment(new Date(flightInfo.departureDate)).format('DD MMM, YYYY'):""}</Text>
            </View>
            </View>
            <View style={styles.horizontalLine} />
            <View style={styles.innerView}>
              <View>
            <Text style={styles.titleStyle}>{"Class"}</Text>
            <Text style={styles.subTitleStyles}>{flightInfo && flightInfo.travelClass ? flightInfo.travelClass :"" }</Text>
            </View>
            <View style={{marginRight:8}}>
            <Text style={styles.titleStyle}>{"Flight Number"}</Text>
            <Text style={styles.subTitleStyles}>{flightInfo && flightInfo.flightNumber ?flightInfo.flightNumber :""}</Text>
            </View>
            </View>
            <View style={styles.horizontalLine} />
            <View style={styles.innerView}>
              <View> 
            <Text style={styles.titleStyle}>{"Airline"}</Text>
            <Text style={styles.subTitleStyles}>{flightInfo && flightInfo.airline ?flightInfo.airline :""}</Text>
            </View>
            <View style={styles.airLineStyle}>
            <Text style={styles.titleStyle}>{"Airline Code"}</Text>
            <Text style={styles.subTitleStyles}>{flightInfo && flightInfo.airlineCode ?flightInfo.airlineCode :""}</Text>
            </View>
            </View>
            <View style={styles.horizontalLine} />
            <View style={styles.innerView}>
              <View>
            <Text style={styles.titleStyle}>{"Duration"}</Text>
            <Text style={styles.subTitleStyles}>{flightInfo && flightInfo.duration && flightInfo.duration != "" ? moment(new Date(flightInfo.duration)).format('HH:mm'):""}</Text>
            </View>
            <View style={styles.pnrStyle}>
            <Text style={styles.titleStyle}>{"PNR"}</Text>
            <Text style={styles.subTitleStyles}>{tripDetail && tripDetail.pnr ?tripDetail.pnr :""}</Text>
            </View>
            </View>
            <View style={styles.horizontalLine} />
            <View style={{alignItems:"center"}}>
            <Text style={styles.titleStyle}>{"Trip Number"}</Text>
            <Text style={styles.subTitleStyles}>{tripDetail && tripDetail.tripNo ?tripDetail.tripNo :""}</Text>
            </View>
        </View>
      ) 
    } else {
      return(
        <View style={{flex:1,justifyContent:'center',alignItems:"center"}}>
             <Text>{`Trip Details are not available on this QR Code!`}</Text>
        </View>
      )
    }
  }
 renderCompleted = () =>{
   return(
     <View style={{flex:1, justifyContent:'center', alignItems:'center', marginBottom:30}}>
         <Animated.Image source={completed} style={{height:150, width:150,transform: [
                            {
                                translateX: this.animatedValue.interpolate({
                                    inputRange: [0, 1],
                                    outputRange: [0, 120]
                                })
                            },
                            {
                                translateY: this.animatedValue.interpolate({
                                    inputRange: [0, 1],
                                    outputRange: [0, 25]
                                })
                            },
                            {
                                scaleX: this.animatedValue.interpolate({
                                    inputRange: [0, 1],
                                    outputRange: [1, 15]
                                })
                            },
                            {
                                scaleY: this.animatedValue.interpolate({
                                    inputRange: [0, 1],
                                    outputRange: [1, 12.5]
                                })
                            }
                        ]}} />
   <Text style={styles.successMsg}>{"Traveller Check In Done Successfully!"}</Text>
     </View>
   )
 }

  success = async () =>{
     let getBoarded = await AsyncStorage.getItem("BOARDED")
     AsyncStorage.setItem("BOARDED",getBoarded ?(eval(getBoarded) + 1).toString() : "1")
  }

  renderButtonView = () =>{
     return(
       <View style={styles.bottomView}>
          <SwipeButton
          title={this.state.isCompleted ? "Done": "Swipe to Check In"}
          railBorderColor={Color.primary}
          railBackgroundColor={Color.primary}
          titleColor={"#FFF"}
          railStyles={{
         }}
          railFillBackgroundColor={"transparent"}
          thumbIconBackgroundColor={"#FFF"}
            onSwipeStart={() => console.log('Swipe started!')}
            onSwipeFail={() => console.log('Incomplete swipe!')}
            onSwipeSuccess={() =>{
              this.setState({isCompleted:true})
              this.success()
            }
             
            }
          />
       </View>
     )
  }
  
     render(){
         const {navigation} = this.props;
         const {isCompleted} = this.state
         const qrData = navigation.getParam('qrCodeData')
         return(
           <View style={styles.container}>
             <Navbar title={"Traveller Detail"} hasBack={true} goBack={(()=>navigation.push("Home"))} />
             {isCompleted? this.renderCompleted() : this.renderTripInfo()}
             {this.renderButtonView()}
           </View>
         )
     }
}

const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: "#EBEBEB",
    },
    titleText:{

    },
    centerText: {
      // flex: 1,
      fontSize: 18,
      padding: 32,
      color: '#777',
    },
    textBold: {
      fontWeight: '500',
      color: '#000',
      marginTop:30,
      fontSize:20
    },
    buttonText: {
      fontSize: 21,
      color: 'rgb(0,122,255)'
    },
    buttonTouchable: {
      padding: 16
    },
    tripInfoMainView:{
      marginHorizontal:16,
      marginVertical:16,
      shadowColor: 'black',
      shadowOffset: { width: 1, height: 1 },
      shadowOpacity: 0.4,
      shadowRadius: 3,
      elevation: 7,
      backgroundColor: 'white',
      borderRadius: 10,
      marginVertical: 5,
      padding:16,
       marginTop:40  
    },
    titleStyle:{
       color:Color.primary,
       fontSize:16,
       marginTop:16,
       fontWeight:'bold'
    },
    subTitleStyles:{
      color:"#A9A9A9",
       fontSize:14,
       marginTop:5,
       
    },
    bottomView:{
      position:'absolute',
      bottom:20,
      right:10,
      left:10,
      shadowColor: 'black',
      shadowOffset: { width: 1, height: 1 },
      shadowOpacity: 0.4,
      shadowRadius: 3,
      elevation: 7,
    },
    innerView:{
      flexDirection:"row",
      justifyContent:"space-between"
    },
    horizontalLine:{
      borderBottomWidth:1,
      borderBottomColor:"#D3D3D3",
      marginTop:10
    },
    successMsg:{
      fontSize:16,
      color:"#000",
      fontWeight:"bold",
      marginTop:50
    },
    pnrStyle:{
      marginRight:60
    },
    airLineStyle:{
      marginRight:19
    },
    departureStyle:{
      marginRight:7
    }
  });

export default Next;