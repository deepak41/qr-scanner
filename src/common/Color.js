export default {
    primary: 'rgb(13,22,47)',
    lightPrimary: '#43A9C7',
    textPrimary: 'rgb(4, 53, 149)',
    red: 'rgb(217,0,36)',
    blue: 'rgb(50, 170, 0)',//'rgb(20,118,224)',
    yellow: 'rgb(253, 144,63)',
    greyButton: 'rgb(216, 216, 216)',
    ButtonRight: 'rgb(95,228,198)',
    mediumGrey: "#3A3A3A",
    blackText:'#000000',
    green:'#0A9500',
    orange:'#F67F33',
    red:'#C80E0E',
    managerHeaderColor:'#732D47',
    yellowButtonColor:'#FCC421',
    primaryBlue: '#101F3E',
    greyColor:'#3A3A3A',
    lightOrange:'rgba(246,127,51,0.2)',
    lightBlue:'rgba(2,74,166,0.2)',
    lightGreen:'rgba(57,144,10,0.2)',
    lightRed:'rgba(200,14,14,0.2)',
    blueDard:'#024AA6',
    lightRed:'#CB1B1B',
    greyBoxColor:'#F2F2F2',
    lightGrey:'#C3C7CF',
    white:'#ffffff',
    


    




}
