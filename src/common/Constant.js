import {Dimensions, Platform} from 'react-native';

const {width, height} = Dimensions.get('window');

const isIphoneX =
  Platform.OS === 'ios' &&
  !Platform.isPad &&
  !Platform.isTVOS &&
  (height === 812 || width === 812);

const isIphoneXS =
  Platform.OS === 'ios' &&
  !Platform.isPad &&
  !Platform.isTVOS &&
  (height === 896 || width === 896);

export default {
  isIphoneX,
  isIphoneXS,
  ToolbarHeight:
    Platform.OS === 'ios' ? (isIphoneX || isIphoneXS ? 35 : 22) : 0,
  navBarHeight: 50,
};
